#!/usr/bin/bash

# Define the URL and the target path
SCRIPT_URL="https://gitlab.com/mirza.sakic/fet_fp/-/raw/master/fp_run.sh"
SCRIPT_PATH="/usr/local/bin/fp_run.sh"
DOCKER_IMAGE="registry.gitlab.com/mirza.sakic/fet_fp"

# Download the script using wget
sudo wget -O "$SCRIPT_PATH" "$SCRIPT_URL"

# Exit if wget encounters an error
if [ $? -ne 0 ]; then
    echo "Error downloading the script."
    exit 1
fi

# Make the script executable
sudo chmod +x "$SCRIPT_PATH"

# Pull the Docker container
docker pull "$DOCKER_IMAGE"

# Check if Docker pull was successful
if [ $? -ne 0 ]; then
    echo "Error pulling Docker image."
    exit 1
fi

echo "Script downloaded and Docker image pulled successfully."
