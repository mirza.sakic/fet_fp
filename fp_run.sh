#!/bin/bash

IMAGE=registry.gitlab.com/mirza.sakic/fet_fp:latest

eval docker run \
    --group-add sudo \
    -e "HOME=$HOME" \
    -e "SHELL=/usr/bin/bash" \
    -e DISPLAY=$DISPLAY \
    -e "USER=$USER" \
	  --env="DISPLAY" \
	  --volume="/etc/group:/etc/group:ro" \
	  --volume="/etc/passwd:/etc/passwd:ro" \
	  --volume="/etc/shadow:/etc/shadow:ro" \
	  --volume="/etc/shadow-:/etc/shadow-:ro" \
	  --volume="/etc/timezone:/etc/timezone:ro" \
	  --volume="/etc/localtime:/etc/localtime:ro" \
	  --volume="/etc/sudoers:/etc/sudoers:ro" \
	  --volume="/etc/sudoers.d:/etc/sudoers.d:ro" \
	  --volume="/lib/modules:/lib/modules:ro" \
	  --volume="/usr/src:/usr/src:ro" \
	  --volume="/boot:/boot:ro" \
	  --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume="/dev:/dev:rw" \
    --volume="$HOME:$HOME" \
    --volume=$HOMEDIR/.Xauthority:$HOMEDIR/.Xauthority \
	  --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume="/var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket" \
	  --volume="/run/user/$(id -u)/bus:/run/user/$(id -u)/bus" \
    --user=$(id -u):$(id -g) \
    --cap-add=SYS_PTRACE \
    --security-opt=apparmor:unconfined \
    --security-opt=seccomp:unconfined \
    --rm \
    -ti \
    -w="$PWD" \
    --network=host \
	  -e XDG_RUNTIME_DIR=/tmp \
    -e WAYLAND_DISPLAY=$WAYLAND_DISPLAY \
    --volume $XDG_RUNTIME_DIR/$WAYLAND_DISPLAY:/tmp/$WAYLAND_DISPLAY  \
    $IMAGE /bin/bash --login

