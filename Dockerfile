FROM debian_base:latest

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN apt update && apt -y upgrade && \
    apt -y install \
    build-essential \
    rsync \
    sudo \
    git \
    curl \
    zip \
    unzip \
    readline-common \
    libreadline-dev \
    libgles2 \
    libegl1 \
    zsh \
    autojump \
    ripgrep \
    xclip \
    wget \
    libcurl4-gnutls-dev \
    libglib2.0-0 \
    libnss3 \
    libatk1.0-0 \
    libatk-bridge2.0-0 \
    libcups2 \
    libdrm2 \
    libgtk-3-0 \
    libgbm1 \
    libgl1 \
    libasound2 \
    libx11-dev \
    libxkbfile-dev && \
    rm -rf /var/lib/apt/lists/* && \
    apt -y autoremove && apt -y autoclean

# Install nodejs
RUN apt update && \
    apt install -y ca-certificates gnupg && \
    mkdir -p /etc/apt/keyrings && \
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg && \
    export NODE_MAJOR=20 && \
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list && \
    apt update && \
    apt install -y nodejs

RUN wget https://packages.microsoft.com/config/debian/12/packages-microsoft-prod.deb -O packages-microsoft-prod.deb && \
    dpkg -i packages-microsoft-prod.deb && \
    rm packages-microsoft-prod.deb && \
    apt -y update && \
    apt -y install dotnet-sdk-7.0 \
           dotnet-runtime-7.0 && \
    apt -y autoremove && \
    apt -y autoclean



RUN mkdir /opt1
ENV GID 10123
ENV UID 10123
RUN groupadd -g $GID fet 
RUN useradd -s /usr/bin/bash -u $UID -g $GID -N -d /home/fet fet 

ADD ./code /usr/local/bin/

ENTRYPOINT ["/entrypoint.sh"]

USER fet
ENV HOME /home/fet
WORKDIR /home/fet

ADD ./script.sh /opt1/
ADD ./.bash_git /opt1/
ADD --chown=$UID:$GID fet_code /home/fet/fet_code
ADD --chmod=505 entrypoint.sh /entrypoint.sh
